package es.uam.eps.dadm.demarco_javier_conecta4.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import es.uam.eps.dadm.demarco_javier_conecta4.R;
import es.uam.eps.dadm.demarco_javier_conecta4.model.RoundRepository;
import es.uam.eps.dadm.demarco_javier_conecta4.model.RoundRepositoryFactory;

public class LoginActivity extends Activity implements View.OnClickListener{
    private RoundRepository repository;
    private EditText usernameEditText;
    private EditText passwordEditText;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        usernameEditText = (EditText) findViewById(R.id.login_username);
        passwordEditText = (EditText) findViewById(R.id.login_password);
        Button loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(this);
        Button cancelButton = (Button) findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(this);
        Button newUserButton = (Button) findViewById(R.id.new_user_button);
        newUserButton.setOnClickListener(this);
        repository = RoundRepositoryFactory.createRepository(LoginActivity.this);
        if (repository == null)
            Toast.makeText(LoginActivity.this, R.string.repository_opening_error
                    , Toast.LENGTH_SHORT).show();
    }
    public void onClick(View v) {
        final String playername = usernameEditText.getText().toString();
        final String password = passwordEditText.getText().toString();
        RoundRepository.LoginRegisterCallback loginRegisterCallback = new
                RoundRepository.LoginRegisterCallback() {
<<<<<<< HEAD
                    @Override public void onLogin(String playerId) {
                        startActivity(new Intent(LoginActivity.this, RoundListActivity.class));
                        finish();
                    }
                    @Override
                    public void
                    onError(String error) {
                        new AlertDialog.Builder(LoginActivity.this)
                                .setTitle(R.string.login_alert_dialog_title)
                                .setMessage(R.string.login_alert_dialog_message)
                                .setNeutralButton(
                                        R.string.login_alert_dialog_neutral_button_text
                                        , new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) { }})
                                .show(); }};
=======
            @Override public void onLogin(String playerId) {
                startActivity(new Intent(LoginActivity.this, RoundListActivity.class));
                finish();
            }
            @Override
            public void
            onError(String error) {
                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle(R.string.login_alert_dialog_title)
                        .setMessage(R.string.login_alert_dialog_message)
                        .setNeutralButton(
                                R.string.login_alert_dialog_neutral_button_text
                                , new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) { }})
                        .show(); }};
>>>>>>> fa9d8f272dc2d55399ed35f3dbc88df3f32e265c
        switch (v.getId()) {
            case R.id.login_button:
                repository.login(playername, password, loginRegisterCallback);
                break;
            case R.id.cancel_button:
                finish();
                break;
            case R.id.new_user_button:
                repository.register(playername, password, loginRegisterCallback);
                break;
        }
    }
}
