package es.uam.eps.dadm.demarco_javier_conecta4.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
<<<<<<< HEAD
import android.support.design.widget.Snackbar;
=======
>>>>>>> fa9d8f272dc2d55399ed35f3dbc88df3f32e265c
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import es.uam.eps.dadm.demarco_javier_conecta4.R;
import es.uam.eps.dadm.demarco_javier_conecta4.model.Round;
import es.uam.eps.dadm.demarco_javier_conecta4.model.RoundRepository;
import es.uam.eps.dadm.demarco_javier_conecta4.model.RoundRepositoryFactory;

/**
 * Created by javi on 31/03/18.
 */

public class RoundListFragment extends Fragment{
    private RecyclerView roundRecyclerView;
    private RoundAdapter roundAdapter;
    private static Callbacks callbacks;

    public interface Callbacks {
        void onRoundSelected(Round round);
        void onPreferencesSelected();
        void onNewRoundAdded(Round round);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callbacks = (Callbacks) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_round:
                final Round round = new Round();
                round.setPlayerUUID(C4PreferenceActivity.getPlayerUUID(getActivity()));
                round.setFirstPlayerName("random");
                round.setSecondPlayerName(C4PreferenceActivity.getPlayerName(getActivity()));
                RoundRepository repository = RoundRepositoryFactory.createRepository(getActivity());
                RoundRepository.BooleanCallback callback = new RoundRepository.BooleanCallback() {
                    @Override
                    public void onResponse(boolean response) {
                        if (response == false)
                            Snackbar.make(getView(), R.string.error_adding_round, Snackbar.LENGTH_LONG).show();
                        else
                            callbacks.onNewRoundAdded(round);
                    }
                };
                repository.addRound(round, callback);
                updateUI();
                return true;
            case R.id.menu_item_settings:
                callbacks.onPreferencesSelected();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_round_list, container, false);
        roundRecyclerView = (RecyclerView) view.findViewById(R.id.round_recycler_view);
        RecyclerView.LayoutManager linearLayoutManager = new
                LinearLayoutManager(getActivity());
        roundRecyclerView.setLayoutManager(linearLayoutManager);
        roundRecyclerView.setItemAnimator(new DefaultItemAnimator());
        updateUI();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    protected void updateUI() {
        List<Round> rounds = new ArrayList<>();
        RoundRepository repository = RoundRepositoryFactory.createRepository(getActivity());
        RoundRepository.RoundsCallback roundsCallback = new RoundRepository.RoundsCallback() {
            @Override
            public void onResponse(List<Round> rounds) {
                if (roundAdapter == null) { roundAdapter =
                        new RoundAdapter(rounds);roundRecyclerView.setAdapter(roundAdapter);
                } else {
                    roundAdapter.setRounds(rounds);
                    roundAdapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onError(String error) {
                Log.d("DEBUG", "No rounds in database");
                            }
        };
        String playeruuid = C4PreferenceActivity.getPlayerUUID(getActivity());
        repository.getRounds(playeruuid, null, null, roundsCallback);
    }

    public static class RoundHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView idTextView;
        private TextView boardTextView;
        private TextView dateTextView;
        private Round round;

        public RoundHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            idTextView = (TextView) itemView.findViewById(R.id.list_item_id);
            boardTextView = (TextView) itemView.findViewById(R.id.list_item_board);
            dateTextView = (TextView) itemView.findViewById(R.id.list_item_date);
        }

        public void bindRound(Round round) {
            this.round = round;
            idTextView.setText(round.getTitle());
            boardTextView.setText(round.getBoard().toString());
            dateTextView.setText(String.valueOf(round.getDate()).substring(0, 19));
        }

        /**
         * Called when a view has been clicked.
         *
         * @param v The view that was clicked.
         */
        @Override
        public void onClick(View v) {
            callbacks.onRoundSelected(round);
        }
    }

    public void onItemClick(View view, final int position) {
        RoundRepository repository = RoundRepositoryFactory.createRepository(getActivity());
        RoundRepository.RoundsCallback roundsCallback = new RoundRepository.RoundsCallback() {
            @Override
            public void onResponse(List<Round> rounds) {
                callbacks.onRoundSelected(rounds.get(position));
            }
            @Override
            public void onError(String error) {
                Snackbar.make(getView(), R.string.error_reading_rounds, Snackbar.LENGTH_LONG).show();
            }
        };
        String playeruuid = C4PreferenceActivity.getPlayerUUID(getActivity());
        repository.getRounds(playeruuid, null, null, roundsCallback);
    }
}
