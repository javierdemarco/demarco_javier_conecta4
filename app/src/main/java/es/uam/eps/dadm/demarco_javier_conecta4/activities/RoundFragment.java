package es.uam.eps.dadm.demarco_javier_conecta4.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import es.uam.eps.dadm.demarco_javier_conecta4.R;
import es.uam.eps.dadm.demarco_javier_conecta4.model.C4Board;
import es.uam.eps.dadm.demarco_javier_conecta4.model.Round;
import es.uam.eps.dadm.demarco_javier_conecta4.model.RoundRepository;
import es.uam.eps.dadm.demarco_javier_conecta4.model.RoundRepositoryFactory;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.JugadorAleatorio;
import es.uam.eps.multij.Partida;
import es.uam.eps.multij.PartidaListener;
import es.uam.eps.multij.Tablero;

/**
 * Created by javi on 31/03/18.
 */

public class RoundFragment extends Fragment implements PartidaListener {
    public static final String DEBUG = "DEBUG";
    public static final String ARG_ROUND_ID = "es.uam.eps.dadm.demarco_javier_conecta4.round_id";
<<<<<<< HEAD
    public static final String ARG_FIRST_PLAYER_NAME = "es.uam.eps.dadm.demarco_javier_conecta4.first_player_name";
    public static final String ARG_ROUND_TITLE = "es.uam.eps.dadm.demarco_javier_conecta4.round_title";
    public static final String ARG_ROUND_DATE = "es.uam.eps.dadm.demarco_javier_conecta4.round_date";
    public static final String ARG_ROUND_BOARD = "es.uam.eps.dadm.demarco_javier_conecta4.round_board";
    private String BOARDSTRING;
    private C4Board board;
=======
    private final int ids[][] = {
            {R.id.er1, R.id.er2, R.id.er3, R.id.er4, R.id.er5, R.id.er6, R.id.er7},
            {R.id.er8, R.id.er9, R.id.er10, R.id.er11, R.id.er12, R.id.er13, R.id.er14},
            {R.id.er15, R.id.er16, R.id.er17, R.id.er18, R.id.er19, R.id.er20, R.id.er21},
            {R.id.er22, R.id.er23, R.id.er24, R.id.er25, R.id.er26, R.id.er27, R.id.er28},
            {R.id.er29, R.id.er30, R.id.er31, R.id.er32, R.id.er33, R.id.er34, R.id.er35},
            {R.id.er36, R.id.er37, R.id.er38, R.id.er39, R.id.er40, R.id.er41, R.id.er42}};
    private Round round;
    private ERBoard board;
>>>>>>> fa9d8f272dc2d55399ed35f3dbc88df3f32e265c
    private Partida game;
    private String roundId, firstPlayerName, roundTitle, roundDate, boardString;
    private Callbacks callbacks;

    @Override
    public void onCambioEnPartida(Evento evento) {
        updateRound();
    }

    public interface Callbacks {
        void onRoundUpdated();
    }

    public static RoundFragment newInstance(String roundId, String firstPlayerName
            , String roundTitle, int roundSize, String roundDate, String roundBoard) {
        Bundle args = new Bundle();
        args.putString(ARG_ROUND_ID, roundId);
        args.putString(ARG_FIRST_PLAYER_NAME, firstPlayerName);
        args.putString(ARG_ROUND_TITLE, roundTitle);
        args.putString(ARG_ROUND_DATE, roundDate);
        args.putString(ARG_ROUND_BOARD, roundBoard);
        RoundFragment roundFragment = new RoundFragment();
        roundFragment.setArguments(args);
        return roundFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(ARG_ROUND_ID)) {
            roundId = getArguments().getString(ARG_ROUND_ID);
        }
        if (getArguments().containsKey(ARG_FIRST_PLAYER_NAME)) {
            firstPlayerName = getArguments().getString(ARG_FIRST_PLAYER_NAME);
        }
        if (getArguments().containsKey(ARG_ROUND_TITLE)) {
            roundTitle = getArguments().getString(ARG_ROUND_TITLE);
        }
        if (getArguments().containsKey(ARG_ROUND_DATE)) {
            roundDate = getArguments().getString(ARG_ROUND_DATE);
        }
        if (getArguments().containsKey(ARG_ROUND_BOARD)) {
            boardString = getArguments().getString(ARG_ROUND_BOARD);
        }
        if (savedInstanceState != null)
            boardString = savedInstanceState.getString(BOARDSTRING);
    }

<<<<<<< HEAD

    private void updateRound() {
        Round round = createRound();
        RoundRepository repository = RoundRepositoryFactory.createRepository(getActivity());
        RoundRepository.BooleanCallback callback = new RoundRepository.BooleanCallback() {
            @Override
            public void onResponse(boolean response) {
                if (response == false)
                    Snackbar.make(getView(), R.string.error_updating_round, Snackbar.LENGTH_LONG).show();
=======
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_round, container,
                false);
        TextView roundTitleTextView = (TextView)
                rootView.findViewById(R.id.round_title);
        roundTitleTextView.setText(round.getTitle());
        view = rootView;
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        startRound();
    }

    private void registerListeners(ERLocalPlayer local) {
        ImageButton button;
        for (int i = 0; i < ERBoard.numFilas; i++)
            for (int j = 0; j < ERBoard.numColumnas; j++) {
                button = view.findViewById(ids[i][j]);
                button.setOnClickListener(local);
            }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    void startRound() {
        ArrayList<Jugador> players = new ArrayList<Jugador>();
        JugadorAleatorio randomPlayer = new JugadorAleatorio("Random player");
        ERLocalPlayer localPlayer = new ERLocalPlayer();

        players.add(localPlayer);
        players.add(randomPlayer);
        if(round.getBoard() != null){
            board = round.getBoard();
        }else {
            board = new ERBoard();
        }
        game = new Partida(board, players);
        game.addObservador(this);
        localPlayer.setPartida(game);
        registerListeners(localPlayer);
        if (game.getTablero().getEstado() == Tablero.EN_CURSO)
            game.comenzar();
    }

    private void updateUI() {
        ImageButton button;
        for (int i = 0; i < ERBoard.numFilas; i++) {
            for (int j = 0; j < ERBoard.numColumnas; j++) {
                button = view.findViewById(ids[i][j]);
                if (board.getTablero(i, j) == ERBoard.JUGADOR1)
                    button.setBackgroundResource(R.drawable.blue_button_48dp);
                else if (board.getTablero(i, j) == ERBoard.VACIO)
                    button.setBackgroundResource(R.drawable.void_button_48dp);
                else
                    button.setBackgroundResource(R.drawable.green_button_48dp);
>>>>>>> fa9d8f272dc2d55399ed35f3dbc88df3f32e265c
            }
        };
        repository.updateRound(round, callback);
    }

<<<<<<< HEAD
    private Round createRound() {
        Round round = new Round();
        round.setPlayerUUID(C4PreferenceActivity.getPlayerUUID(getActivity()));
        round.setId(roundId);
        round.setFirstPlayerName("random");
        round.setSecondPlayerName(firstPlayerName);
        round.setDate(roundDate);
        round.setTitle(roundTitle);
        round.setBoard(board);
        return round;
=======
    @Override
    public void onCambioEnPartida(Evento evento) {
        switch (evento.getTipo()) {
            case Evento.EVENTO_CAMBIO:
                updateUI();
                if(round != null && callbacks != null){
                    callbacks.onRoundUpdated(round);
                }
                if (board.winCondition()){
                    new AlertDialogFragment().show(getActivity().getSupportFragmentManager(),"ALERT DIALOG");
                }
                break;
            case Evento.EVENTO_FIN:
                updateUI();
                if(round != null && callbacks != null){
                    callbacks.onRoundUpdated(round);
                }
                new AlertDialogFragment().show(getActivity().getSupportFragmentManager(),"ALERT DIALOG");
                break;
        }
>>>>>>> fa9d8f272dc2d55399ed35f3dbc88df3f32e265c
    }
}