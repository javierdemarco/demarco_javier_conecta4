/*
 * TableroConecta4.java
 *
 * Created on February 7, 2016, 14:30
 * Autor: Javier de Marco
 */

package es.uam.eps.dadm.demarco_javier_conecta4.model;

import java.util.ArrayList;
import java.util.Arrays;

import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Movimiento;
import es.uam.eps.multij.Tablero;

/**
 * Clase de C4Board. Clase que representa un tablero del juego de
 * Conecta4.
 *
 * @author Javier de Marco
 */
public class C4Board extends Tablero {
    /**
     * Parametro array con las dimensiones del C4Board El primer numero
     * indica la fila, y el segundo la columna. Por defecto 6 filas y 7 columnas.
     * Dentro de las dimensiones, si el valor es 0, no ha ficha si hay un 1, la
     * casilla esta ocupada por el jugador 1, y 2 por el Jugador 2.
     */
    private int[][] dim;

    /**
     * Variable auxiliar que se utiliza para indicar un error.
     */
    private static final String ERR = "-1";
    /**
     * Variable estatica final para indicar jugador 1
     */
    public static final int JUGADOR1 = 1;
    /**
     * Variable estatica final para indicar jugador 2
     */
    public static final int JUGADOR2 = 2;
    /**
     * Variable estatica final para indicar Vacio
     */
    public static final int VACIO = 0;
    /**
     * Variable estatica final para indicar Numero de filas
     */
    public static final int numFilas = 6;
    /**
     * Variable estatica final para numero de columnas
     */
    public static final int numColumnas = 7;

    /**
     * Constructor por defecto de tableroConecta4
     */
    public C4Board() {
        super();
        this.setTurno(JUGADOR1);
        this.dim = new int[numFilas][numColumnas];
        this.setEstado(Tablero.EN_CURSO);
        this.reset();
    }

    /**
     * Metodo que setea diversas variables del tablero
     *
     * @param fils       Filas que contiene
     * @param cols       Columnas que contiene
     * @param estado     Estado del tablero
     * @param numJugadas Numero de jugadas realizadas
     * @param turno      Turno de la partida
     * @param ultMov     Ultimo movimiento
     */
    private void setTableroConecta4(int fils, int cols, int estado, int numJugadas, int turno, int ultMov) {
        this.setTurno(turno);
        this.dim = new int[fils][cols];
        this.setEstado(estado);
        this.setUltimoMovimiento(ultMov);
        this.setNumJugadas(numJugadas);
    }

    /**
     * Metodo getter de las dimensiones
     *
     * @return int[][] con las dimensiones
     */
    public int[][] getDim() {
        return dim;
    }

    /**
     * Metodo setter para setear las dimensiones
     *
     * @param dim Dimensiones
     */
    @SuppressWarnings("unused")
    private void setDim(int[][] dim) {
        this.dim = dim;
    }

    /**
     * Getter del ultimo movimeinto
     *
     * @return Movimiento con el ultimo movimiento
     */
    public Movimiento getUltimoMovimiento() {
        return this.ultimoMovimiento;
    }

    /**
     * Setter del Ultimo movimeinto
     *
     * @param mov String con el movimiento
     */
    private void setUltimoMovimiento(int mov) {
        C4Movement ultm = new C4Movement(mov);
        this.ultimoMovimiento = ultm;
    }

    /**
     * Setter del Utlimo movimientno
     *
     * @param mov Mocimiento con el movimiento a setear
     */
    private void setUltimoMovimiento(Movimiento mov) {
        this.ultimoMovimiento = mov;
    }

    /**
     * Setter del turno de la clase C4Board
     *
     * @param i turno al que se pondra el tablero
     */
    private void setTurno(int i) {
        if (i == JUGADOR1 || i == JUGADOR2) {
            this.turno = i - 1;
        }
    }

    /**
     * Setter del estado de la clase C4Board
     *
     * @param i estado al que se pondra el tablero
     */
    private void setEstado(int i) {
        if (i == Tablero.EN_CURSO || i == Tablero.FINALIZADA || i == Tablero.TABLAS) {
            this.estado = i;
        }
    }

    /**
     * Setter para el parametro de numero de jugadas del tablero.
     *
     * @param i numero al que se pondra el numero de jugadas.
     */
    private void setNumJugadas(int i) {
        this.numJugadas = i;
    }

    /**
     * Metodo que mueve en un tablero
     *
     * @param m Objeto de clase Movimiento que indica el movimiento que se realiza
     * @throws ExcepcionJuego                 si el movimiento no es correcto
     * @throws ArrayIndexOutOfBoundsException Si el movimiento se sale de la matriz
     */
    @Override
    protected void mueve(Movimiento m) throws ExcepcionJuego, ArrayIndexOutOfBoundsException {
        int[] mov;
        mov = new int[2];
        /* Pruebo a mover si no lanzo la excepcion */
		/* Compruebo que sea valido */
        if (this.esValido(m)) {
			/* Compruebo si el tablero ya se ha ganado */
            if (!this.winCondition()) {
				/* Obtengo las coordenadas del movimiento */
                mov = this.vacioEnColumna(((C4Movement) m).getCol());
				/* Coloco la ficha */
                this.setFicha(mov[0], mov[1]);
				/* Aumento el numero de jugadas */
                this.setNumJugadas(this.getNumJugadas() + 1);
				/* Cambio el turno */
                this.cambioTurno();
				/* Seteo el ultimo movimiento */
                this.setUltimoMovimiento(m);
                if (this.movimientosValidos().isEmpty()) { /* Si no hay movimiento es tablas */
                    this.setEstado(TABLAS);
                    return;
                }
            } else { /*
						 * Si se ha ganado seteo el estado y cambio turno para notificar corretamente el
						 * ganador
						 */
                this.setEstado(FINALIZADA);
            }
        } else {
            ExcepcionJuego e = new ExcepcionJuego(m.toString());
            throw (e);
        }
    }

    /**
     * Metodo que cambia de turno
     */
    private void cambioTurno() {
        if (this.getTurno() == JUGADOR1 - 1) {
            this.setTurno(JUGADOR2);
        } else {
            this.setTurno(JUGADOR1);
        }
    }

    /**
     * Metodo privado que "Coloca" una ficha en unas coordenadas especificas
     *
     * @param fil fila donde colocar la ficha
     * @param col columna donde colocar la ficha
     */
    private void setFicha(int fil, int col) {
        if (this.getTurno() == JUGADOR1 - 1) {
            this.dim[fil][col] = JUGADOR1;
        } else {
            this.dim[fil][col] = JUGADOR2;
        }
    }

    /**
     * Metodo que indica si un movimiento es valido o no.
     *
     * @param m Objeto de tipo movimiento en el cual se comprobara si es valido o
     *          no
     * @return True si es valido, False si no.
     */
    @Override
    public boolean esValido(Movimiento m) {
        int col;
		/* Si es null, o no es instancia de un movimiento de conecta 4 no sirve */
        if (m == null || !(m instanceof C4Movement)) {
            return false;
        }
		/* Obtengo la columna del movimiento */
        col = ((C4Movement) m).getCol();
		/*
		 * Si en dicha columna hay un hueco donde poder colocar una ficha el movimiento
		 * es valido, si no falso.
		 */
        return (this.vacioEnColumna(col) != null) ? true : false;
    }

    /**
     * Metodo que devuelve los movimientos validos dentro del tablero.
     *
     * @return ArrayList con los movimientos validos.
     */
    @Override
    public ArrayList<Movimiento> movimientosValidos() {
		/* Variable de retorno */
        ArrayList<Movimiento> ret = new ArrayList<Movimiento>();
		/* Itero las columnas */
        for (int i = 0; i < dim[0].length; i++) {
			/*
			 * Para encontrar posiciones vacias donde se pueden colocar ficha En resumen un
			 * movimiento valido
			 */
            if (this.vacioEnColumna(i) != null) {
				/*
				 * Si esa columna es adecuada para realizar un movimiento lo a�ado a la lista
				 */
                ret.add((Movimiento) new C4Movement(i));
            }
        }
        return ret;
    }

    /**
     * Metodo que transforma un tablero en un string para poder guardar la partida.
     *
     * @return String con una codificacion en string del tablero
     */
    @Override
    public String tableroToString() {
        String tab = new String();
        tab += this.dim.length; /* Numeor de filas */
        tab += ":";
        tab += this.dim[0].length;/* Numero de columnas */
        tab += ":";
        tab += this.getEstado(); /* Estado */
        tab += ":";
        tab += this.getNumJugadas(); /* Numero de Jugadas */
        tab += ":";
        tab += this.getTurno(); /* Turno */
        tab += ":";
        if (this.getUltimoMovimiento() != null) {
            tab += this.getUltimoMovimiento().toString(); /* Ultimo movimiento */
        } else {
            tab += ERR;
        }
        tab += ":";
        for (int[] row : this.dim) {
            for (int col : row) {
                tab += col;
                tab += ":";
            }
        }
        return tab;
    }

    /**
     * Metodo que convierte una cadena en un tablero para poder continuar jugando.
     *
     * @param cadena String con el tablero codificado.
     * @throws ExcepcionJuego Exception que controla los fallos en el juego
     */
    @Override
    public void stringToTablero(String cadena) throws ExcepcionJuego {
        try {
            this.checkStringToTablero(cadena);
        } catch (ExcepcionJuego e) {
            throw (e);
        }
        String[] result = cadena.split(":");

        if (!result[5].equals(ERR)) {
            String[] mov = result[5].split("_");
            this.setTableroConecta4(Integer.parseInt(result[0]), Integer.parseInt(result[1]), Integer.parseInt(result[2]),
                    Integer.parseInt(result[3]), Integer.parseInt(result[4]), Integer.parseInt(mov[1]));
        } else {

            this.setTableroConecta4(Integer.parseInt(result[0]), Integer.parseInt(result[1]), Integer.parseInt(result[2]),
                    Integer.parseInt(result[3]), Integer.parseInt(result[4]), Integer.parseInt(result[5]));
        }
        int k = 0;
        for (int i = 0; i < this.dim.length; i++) {
            for (int j = 0; j < this.dim[0].length; j++) {
                this.dim[i][j] = Integer.parseInt(result[k + 6]);
                k++;
            }
        }

    }

    /**
     * Metodo de string de tablero. Convierte a string un tablero.
     *
     * @return String con el tablero convertido a string
     */
    @Override
    public String toString() {
        String tab = "";
        /*tab += "Estado = ";
        if(this.getEstado() == 1){
            tab += "En Curso ";
        }else if (this.getEstado() == 2){
            tab += "Finalizada ";
        }else {
            tab += "Tablas ";
        }
        tab += "\n";
        tab += "Turno Jugador = " + this.getTurno() + "\n";
        tab += "Numero de Jugadas = " + this.getNumJugadas() + "\n";
        if (this.getUltimoMovimiento() != null) {
            tab += "Ultimo Movimiento = " + this.getUltimoMovimiento().toString() + "\n";
        }*/
        for (int[] is : dim) {
            for (int i : is) {
                tab += (" " + i + " ");
            }
            tab += ("\n");
        }
        return tab;
    }

    /**
     * Metodo que resetea el tablero al estado inicial.
     *
     * @return True si ha sido realizado, false si no
     */
    @Override
    public boolean reset() {
        this.setNumJugadas(0);
        this.setEstado(Tablero.EN_CURSO);
        this.setTurno(JUGADOR1);
        this.liberarTablero();
        return true;
    }

    /**
     * Metodo que libera el tablero de fichas, poniendo cada casilla a 0, indicando
     * VACIO.
     */
    private void liberarTablero() {
        for (int i = 0; i < this.dim.length; i++) {
            Arrays.fill(this.dim[i], VACIO);
        }
    }

    /**
     * Metodo para encontrar la pasicion en la columna indicada en entrada para
     * poder colocar una ficha.
     *
     * @param col_in la columna donde se desea encontrar el lugar donde colocar la
     *               ficha
     * @return ret la posicion donde poder colocar la ficha, null si no se ha
     * encontrado ninguna posicion. y la columna esta llena.
     */
    private int[] vacioEnColumna(int col_in) {
		/* Variable de retorno */
        int[] ret = new int[2];
        ret[0] = -1;
        ret[1] = -1;
		/*
		 * Se itera desde la fila mas abajo, hacia arriba en la columna indicada en la
		 * entrada y se encuentra el lugar mas profundo libre
		 */
        for (int row = dim.length - 1; row >= 0; row--) {
            if (dim[row][col_in] == VACIO) {
                ret[0] = row;
                ret[1] = col_in;
                break;
            }
        }
		/*
		 * Si no se encontro seguiran con los valores por defecto por lo que devuelvo
		 * null
		 */
        if (ret[0] == -1 || ret[1] == -1) {
            return null;
        }
        return ret;
    }

    /**
     * Metodo de comprobacion de si un tablero esta en posicion de ganada
     *
     * @return True si lo esta, False si no
     */
    public boolean winCondition() {
        int player = 0;
		/* Horizontal */
        for (int row = 0; row < this.dim.length; row++) {
            for (int col = 0; col < this.dim[0].length - 3; col++) {
                if (this.dim[row][col] != VACIO) {
                    player = this.dim[row][col];
                    if (this.dim[row][col + 1] == player && this.dim[row][col + 2] == player
                            && this.dim[row][col + 3] == player) {
                        return true;
                    }
                }
            }
        }
		/* Vertical */
        for (int row = 0; row < this.dim.length - 3; row++) {
            for (int col = 0; col < this.dim[0].length; col++) {
                if (this.dim[row][col] != VACIO) {
                    player = this.dim[row][col];
                    if (this.dim[row + 1][col] == player && this.dim[row + 2][col] == player
                            && this.dim[row + 3][col] == player) {
                        return true;
                    }
                }
            }
        }
		/* Diagonal hacia abajo */
        for (int row = 0; row < this.dim.length - 3; row++) {
            for (int col = 0; col < this.dim[0].length - 3; col++) {
                if (this.dim[row][col] != VACIO) {
                    player = this.dim[row][col];
                    if (this.dim[row + 1][col + 1] == player && this.dim[row + 2][col + 2] == player
                            && this.dim[row + 3][col + 3] == player) {
                        return true;
                    }
                }
            }
        }
		/* Diagonal hacia arriba */
        for (int row = this.dim.length - 1; row >= 3; row--) {
            for (int col = 0; col < this.dim[0].length - 3; col++) {
                if (this.dim[row][col] != VACIO) {
                    player = this.dim[row][col];
                    if (this.dim[row - 1][col + 1] == player && this.dim[row - 2][col + 2] == player
                            && this.dim[row - 3][col + 3] == player) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Metodo que checkea si una cadena es correcta para cargar.
     *
     * @param s Cadena a analizar
     * @throws ExcepcionJuego Excepcion con los diferentes mensajes de fallos
     */
    public void checkStringToTablero(String s) throws ExcepcionJuego {
        String[] result = s.split(":");
        try {
			/* Primero miro si las dimensiones son correctas */
            if (Integer.parseInt(result[0]) != numFilas || Integer.parseInt(result[1]) != numColumnas) {
                ExcepcionJuego e = new ExcepcionJuego("La cadena a cargar no tiene las dimensiones correctas.\n");
                throw (e);
            }
            int dims = this.dim.length * this.dim[0].length;
            if (dims != result.length - 6) {
                throw (new ArrayIndexOutOfBoundsException());
            }
			/* Creo una tabla auxiliar para comprobar el estado */
            int k = 0;
            C4Board taux = new C4Board();
            for (int i = 0; i < taux.dim.length; i++) {
                for (int j = 0; j < taux.dim[0].length; j++) {
                    taux.dim[i][j] = Integer.parseInt(result[k + 6]);
                    k++;
                }
            }
            int state = Integer.parseInt(result[2]);
            if (taux.winCondition()) { /* Si esta en estado de finalizada */
                if (state != Tablero.FINALIZADA) { /* Pero el estado en la cadena no es finalizado */
                    ExcepcionJuego e = new ExcepcionJuego(
                            "La cadena a cargar tiene un estado incorreto. (Tablero = Finalizado, Cadena != Finalizado)\n");
                    throw (e);
                }
            } else { /* No esta en estado finalizado, puede estar en curso o tablas */
                if (taux.movimientosValidos().isEmpty()) { /* Si esta en tablas */
                    if (state != Tablero.TABLAS) { /* Pero la cadena no */
                        ExcepcionJuego e = new ExcepcionJuego(
                                "La cadena a cargar tiene un estado incorreto. (Tablero = Tablas, Cadena != Tablas)\n");
                        throw (e);
                    }
                } else { /* Si esta en curso */
                    if (state != Tablero.EN_CURSO) { /* Pero la cadena no */
                        ExcepcionJuego e = new ExcepcionJuego(
                                "La cadena a cargar tiene un estado incorreto. (Tablero = En Curso, Cadena != En Curso)\n");
                        throw (e);
                    }
                }
            }
			/* Comporbacion numero de jugadas */
            int numJugadas = Integer.parseInt(result[3]);
            int count = 0;
            for (k = 6; k < result.length; k++) {
                if (Integer.parseInt(result[k]) != VACIO) {
                    count++;
                }
            }
            if (count != numJugadas) {
                ExcepcionJuego e = new ExcepcionJuego(
                        "El numero de jugadas realizadas en la cadena y el tablero no coinciden.\n");
                throw (e);
            }
			/* Turno */
            if ((numJugadas % 2) == 0) { /* Si el numero de jugadas es par */
                if (Integer.parseInt(result[4]) != JUGADOR1 - 1) { /* El turno es del jugador 1 */
                    ExcepcionJuego e = new ExcepcionJuego("El Turno de la cadena es incorrecto.\n");
                    throw (e);
                }
            } else {/* Si es impar */
                if (Integer.parseInt(result[4]) != JUGADOR2 - 1) { /* El turno es del jugador 2 */
                    ExcepcionJuego e = new ExcepcionJuego("El Turno de la cadena es incorrecto.\n");
                    throw (e);
                }
            }
			/* Comprobacion del ultimo movimiento */
            if (!result[5].equals(ERR)) {
                String[] movUlt = result[5].split("_");
                if (!movUlt[0].equals("C")) {
                    ExcepcionJuego e1 = new ExcepcionJuego(
                            "La cadena a cargar tiene el ultimo movimiento mal escrito.\n");
                    throw (e1);
                }
                int mov = 0;
                try {
                    mov = Integer.parseInt(movUlt[1]);
                } catch (ArrayIndexOutOfBoundsException e) {
                    ExcepcionJuego e1 = new ExcepcionJuego("El ultimo movimiento es incorrecto.\n");
                    throw (e1);
                }
				/*
				 * El ultimo numero que haya en la columna indicada tiene que ser del otro
				 * jugador
				 */
                if (Integer.parseInt(result[4]) == JUGADOR2 - 1) {/* Si el turno es del jugador 2 */
					/* Tiene que haber un 1 */
					/* Itero en la columna del ultimo movimiento */
                    int row = 0;
                    for (row = taux.dim.length - 1; row >= 0; row--) {
                        if (taux.dim[row][mov] == VACIO) { /* Busco un VACIO */
                            try {
                                if (taux.dim[row + 1][mov] != JUGADOR1) {/* El que este debajo del VACIO es el ultimo movimiento */
									/* Si no es igual a 1 entonces error */
                                    ExcepcionJuego e = new ExcepcionJuego("El utlimo movimiento es incorrecto.\n");
                                    throw (e);
                                }
                            } catch (ArrayIndexOutOfBoundsException e) {
                                ExcepcionJuego e1 = new ExcepcionJuego("El utlimo movimiento es incorrecto.\n");
                                throw (e1);
                            }
                            break;
                        }
                    }
					/*
					 * Si sale del bucle es porque la columna esta llena por lo que tendremos que
					 * mirar el que mas arriba esta
					 */
                    if (taux.dim[row + 1][mov] != JUGADOR1) {
                        ExcepcionJuego e = new ExcepcionJuego("El utlimo movimiento es incorrecto.\n");
                        throw (e);
                    }
                } else {/* Si tiene que el turno el jugador 1 */
					/* Tiene que haber un 2 */
					/* Itero en la columna del ultimo movimiento */
                    int row = 0;
                    for (row = taux.dim.length - 1; row >= 0; row--) {
                        if (taux.dim[row][mov] == VACIO) { /* Busco un VACIO */
                            try {
                                if (taux.dim[row + 1][mov] != JUGADOR2) {/* El que este debajo del VACIO es el ultimo movimiento */
									/* Si no es igual a 1 entonces error */
                                    ExcepcionJuego e = new ExcepcionJuego("El utlimo movimiento es incorrecto.\n");
                                    throw (e);
                                }
                            } catch (ArrayIndexOutOfBoundsException e) {
                                ExcepcionJuego e1 = new ExcepcionJuego("El utlimo movimiento es incorrecto.\n");
                                throw (e1);
                            }
                            break;
                        }
                    }
					/*
					 * Si sale del bucle es porque la columna esta llena por lo que tendremos que
					 * mirar el que mas arriba esta
					 */
                    if (taux.dim[row + 1][mov] != JUGADOR2) {
                        ExcepcionJuego e = new ExcepcionJuego("El utlimo movimiento es incorrecto.\n");
                        throw (e);
                    }
                }
            }
        } catch (NumberFormatException e) { /* Fallo de formato */
            ExcepcionJuego e1 = new ExcepcionJuego("La cadena a cargar tiene letras.\n");
            throw (e1);
        } catch (ArrayIndexOutOfBoundsException e) { /* Demasiados o pocos numeros en la cadena */
            ExcepcionJuego e1 = new ExcepcionJuego("La cadena a cargar despues de las variables de configuracion, "
                    + " tiene mas/menos numeros de los necesarios.\n");
            throw (e1);
        }
        return;
    }

    public int getTablero(int i, int j) {
        return dim[i][j];
    }
}
