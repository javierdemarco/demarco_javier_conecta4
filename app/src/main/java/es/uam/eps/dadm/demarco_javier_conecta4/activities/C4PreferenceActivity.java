package es.uam.eps.dadm.demarco_javier_conecta4.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;

import es.uam.eps.dadm.demarco_javier_conecta4.R;

public class C4PreferenceActivity extends AppCompatActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        FragmentManager fm = getSupportFragmentManager();
        C4PreferenceFragment fragment = new C4PreferenceFragment();
        fm.beginTransaction().replace(android.R.id.content, fragment).commit();
    }

    public static String getPlayerUUID(FragmentActivity activity) {
        return "HOLA";
    }

    public static String getPlayerName(FragmentActivity activity) {
        return "HOLA";
    }
}
