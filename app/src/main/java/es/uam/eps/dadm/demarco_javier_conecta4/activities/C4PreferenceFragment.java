package es.uam.eps.dadm.demarco_javier_conecta4.activities;

import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;

import es.uam.eps.dadm.demarco_javier_conecta4.R;

public class C4PreferenceFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootkey){
        setPreferencesFromResource(R.xml.settings, rootkey);
    }
}
