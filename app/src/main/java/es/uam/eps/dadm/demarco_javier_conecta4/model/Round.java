package es.uam.eps.dadm.demarco_javier_conecta4.model;

/**
 * Created by javi on 16/03/18.
 */

import java.util.Date;
import java.util.UUID;

public class Round {
    private String id;
    private String title;
    private String date;
<<<<<<< HEAD
    private C4Board board;
=======
    private ERBoard board;
>>>>>>> fa9d8f272dc2d55399ed35f3dbc88df3f32e265c
    private String firstPlayerName;
    private String secondPlayerName;

    public Round() {
        id = UUID.randomUUID().toString();
        title = "ROUND " + id.toString().substring(19, 23).toUpperCase();
        date = new Date().toString();
        board = new C4Board();
    }

    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void
    setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public C4Board getBoard() {
        return board;
    }

    public void setBoard(C4Board board) {
        this.board = board;
    }

    public String getPlayerUUID() {
        return id;
    }

    public void setFirstPlayerName(String firstPlayerName) {
        this.firstPlayerName = firstPlayerName;
    }

    public void setSecondPlayerName(String secondPlayerName) {
        this.secondPlayerName = secondPlayerName;
    }

    public void setPlayerUUID(String playerUUID) {
        this.id = playerUUID;
    }
}