/*
 * MovimientoConecta4.java
 *
 * Created on February 7, 2016, 14:30
 */

package es.uam.eps.dadm.demarco_javier_conecta4.model;

/**
 * Clase C4Movement. Esta clase representa un moviemiento en el juego de
 * Conecta4.
 * 
 * @author Javier de Marco
 */
public class C4Movement extends es.uam.eps.multij.Movimiento {
	/**
	 * Parametro de la clase C4Movement, que representa la columna en la que
	 * se realiza el movimiento.
	 */
	int col;

	/**
	 * Constructor C4Movement
	 * 
	 * @param col
	 *            la columna del movimiento donde se posara la ficha
	 */
	public C4Movement(int col) {
		super();
		this.setCol(col);
	}

	/**
	 * Metodo ToString. Convierte un movimiento en un string. del modod C#, donde #
	 * es el numero de la columna donde se haya dejado caer la ficha.
	 * 
	 * @return String string con el movimiento.
	 */
	@Override
	public String toString() {
		return "C_" + col;
	}

	/**
	 * Metodo equals de la clase C4Movement Compara dos objetos viendo si
	 * son iguales o no.
	 * 
	 * @param o
	 *            Objeto con el que se comparara.
	 * @return True si son iguales. False si no.
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (o == this) {
			return true;
		}
		if (o instanceof C4Movement && ((C4Movement) o).getCol() == this.getCol()) {
			return true;

		}
		return false;
	}

	/**
	 * Getter de la columna del moviemiento
	 * 
	 * @return int con el numero de la columna del movimiento
	 */
	public int getCol() {
		return col;
	}

	/**
	 * Setter de la columna del movimiento
	 * 
	 * @param col
	 *            columna a la que se inicializara el movimiento
	 */
	private void setCol(int col) {
		this.col = col;
	}

}
