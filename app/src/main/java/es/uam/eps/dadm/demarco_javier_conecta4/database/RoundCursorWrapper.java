package es.uam.eps.dadm.demarco_javier_conecta4.database;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;

import es.uam.eps.dadm.demarco_javier_conecta4.model.Round;
import es.uam.eps.multij.ExcepcionJuego;

import static es.uam.eps.dadm.demarco_javier_conecta4.BuildConfig.DEBUG;

public class RoundCursorWrapper extends CursorWrapper {
    public RoundCursorWrapper(Cursor cursor) {
        super(cursor);
    }
    public Round getRound() {
        String playername = getString(getColumnIndex(RoundDataBaseSchema.UserTable.Cols.PLAYERNAME));
        String playeruuid = getString(getColumnIndex(RoundDataBaseSchema.UserTable.Cols.PLAYERUUID));
        String rounduuid = getString(getColumnIndex(RoundDataBaseSchema.RoundTable.Cols.ROUNDUUID));
        String date = getString(getColumnIndex(RoundDataBaseSchema.RoundTable.Cols.DATE));
        String title = getString(getColumnIndex(RoundDataBaseSchema.RoundTable.Cols.TITLE));
        String board = getString(getColumnIndex(RoundDataBaseSchema.RoundTable.Cols.BOARD));
        Round round = new Round();
        round.setFirstPlayerName("random");
        round.setSecondPlayerName(playername);
        round.setPlayerUUID(playeruuid);
        round.setId(rounduuid);
        round.setDate(date);
        round.setTitle(title);
        try {
            round.getBoard().stringToTablero(board);
        }
        catch (ExcepcionJuego e) {
            Log.d(String.valueOf(DEBUG), "Error turning string into tablero");
        }
        return round;
    }
}
