package es.uam.eps.dadm.demarco_javier_conecta4.activities;

import android.support.design.widget.Snackbar;
import android.view.View;

import es.uam.eps.dadm.demarco_javier_conecta4.R;
import es.uam.eps.dadm.demarco_javier_conecta4.model.C4Board;
import es.uam.eps.dadm.demarco_javier_conecta4.model.C4Movement;
import es.uam.eps.multij.AccionMover;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.Partida;
import es.uam.eps.multij.Tablero;


public class C4LocalPlayer implements View.OnClickListener, Jugador {
    private final int ids[][] = {
            {R.id.er1, R.id.er2, R.id.er3, R.id.er4, R.id.er5, R.id.er6, R.id.er7},
            {R.id.er8, R.id.er9, R.id.er10, R.id.er11, R.id.er12, R.id.er13, R.id.er14},
            {R.id.er15, R.id.er16, R.id.er17, R.id.er18, R.id.er19, R.id.er20, R.id.er21},
            {R.id.er22, R.id.er23, R.id.er24, R.id.er25, R.id.er26, R.id.er27, R.id.er28},
            {R.id.er29, R.id.er30, R.id.er31, R.id.er32, R.id.er33, R.id.er34, R.id.er35},
            {R.id.er36, R.id.er37, R.id.er38, R.id.er39, R.id.er40, R.id.er41, R.id.er42}};

    Partida game;
    String name;

    public C4LocalPlayer() {
        /*for (int[] row: ids) {
            Arrays.fill(row, 0);
        }*/
        name = "Local Player";
    }

    public void setPartida(Partida game) {
        this.game = game;
    }
    @Override
    public String getNombre() {
        return "Local player";
    }
    @Override
    public boolean puedeJugar(Tablero tablero) {
        return true;
    }
    @Override
    public void onCambioEnPartida(Evento evento) {
    }

    private int fromViewToI(View view) {
        for (int i = 0; i < C4Board.numFilas; i++)
            for (int j = 0; j < C4Board.numColumnas; j++)
                if (view.getId() == ids[i][j])
                    return j;
        return -1;
    }

    @Override
    public void onClick(View v) {
        try {
            if (game.getTablero().getEstado() != Tablero.EN_CURSO) {
                Snackbar.make(v, R.string.round_already_finished,
                        Snackbar.LENGTH_SHORT).show();
                return;
            }
            C4Movement m;
            m = new C4Movement(fromViewToI(v));
            game.realizaAccion(new AccionMover(this, m));
        } catch (Exception e) {
            Snackbar.make(v, R.string.invalid_movement, Snackbar.LENGTH_SHORT).show();
        }
    }
}
